<?php

namespace ApiController;

class Api extends \Controller {

  private $model;
  private $api;
  private $table;
  private $id = null;
  private $where_id = null;
  private $last_id;
  private $order;
  private $limit;

  /**
   * API header prefix usage: `{$prefix}-api-key` & `{$prefix}-api-password`, ...
   * @var string
   */
  private $prefix = 'ORNATA'; // customizable at your desire, capital only with underscores.

  public function __construct() {
    parent::__construct();

    $this->load_model("ApiModel\Apis");
    $this->model = new \ApiModel\Apis;
    $this->table = sanitizer($_GET['table'] ?? '');

    $this->auth();
  }

  /**
   * accepting api key and password using `HTTP_{$prefix}_API_KEY` and `HTTP_{$prefix}_API_PASSWORD`
   * else; kill the request
   */
  private function auth() {
    $headers = array_key_exists("HTTP_{$this->prefix}_API_KEY", $_SERVER) and
      array_key_exists("HTTP_{$this->prefix}_API_PASSWORD", $_SERVER);

    if (!$headers) {
      $this->kill("Authentication failed. No credentials provided.", 401);
    }
    else {
      $api_key = $_SERVER["HTTP_{$this->prefix}_API_KEY"] ?? null;
      $api_pwd = $_SERVER["HTTP_{$this->prefix}_API_PASSWORD"] ?? null;
      $api = $this->model->get(where: ['api_key' => $api_key, 'api_password' => $api_pwd]);

      if (count($api) == 0) {
        $this->kill("Authentication failed. Credentials don't match.", 403);
      }
      else {
        $this->api = (object) current($api);

        if ($this->api->api_is_active == 0) {
          $this->kill("Authentication failed. Credentials deactivated by the administrator", 403);
        }
        else if ($this->api->api_date_expired) {
          $d_expired = date("Y-m-d", strtotime($this->api->api_date_expired));

          if (date("Y-m-d") > $d_expired) {
            $this->kill("Authentication failed. API has expired.", 403);
          }
        }
        else {
          // All authorization tasks succeeded.

          // 1. check if requested $this->table is permitted
          if ($this->table and !in_array($this->table, (array) $this->api->api_permissions)) {
            $this->kill("Unauthorized request. Accessing the table `{$this->table}` is forbidden.", 403);
          }
        }
      }
    }
  }

  public function tables() {
    echo $this->make((array) $this->api->api_permissions);
  }

  private function kill(string $message, int $code = 422) {
    http_response_code($code);
    echo json_encode(['status' => 'error', 'code' => $code, 'reason' => $message]);
    die();
  }

  protected function make(array|object|string|int $data) {
    return json_encode(['status' => 'ok', 'code' => 200, 'data' => $data]);
  }

}