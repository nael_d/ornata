<?php

class Encrypia {

  private static
    $key,
    $level = 1;

  public static function getKey() {
    return self::$key ?? null;
  }

  public static function setKey(string $key, int|null $level = null) {
    $key_mod    = '';
    $key_return = '';
    $key_split  = str_split($key);
    $level      = $level ?? self::$level;

    $key_return = (is_numeric($key))
      ? $key
      : join('', array_map(function($l) use($key_mod) {
        $ll = intval(ord($l) / 1) . ',';
        if (strlen($ll) > 1) { $key_mod .= array_sum(str_split($ll)); }
        return $key_mod;
      }, $key_split));

    self::$key    = $key_return;
    self::$level  = $level;
  }

  public static function setLevel(int $level) {
    self::$level = $level;
  }

  public static function blind(string|null $text = null, string|null $key = null) {
    if (!$text) return;

    $key = $key ?? self::$key;

    if ($key) {
      if (self::$level == 1) {
        $text       = str_split($text);
        $key_split  = str_split($key);
        $key_count  = count($key_split);
        $result_arr = [];
        $i          = 0;

        foreach ($text as $k => $value) {
          $new_chr = chr(ord($value) + $key_split[$i]);
          $result_arr[] = $new_chr;
          $i = ($i + 1) == $key_count ? 0 : $i + 1;
        }

        return join('', $result_arr);
      }
      else if (self::$level == 2) {
        //
      }
    }
    else {
      die("Encrypia error: no key is set.");
    }
  }

  public static function unblind(string|null $text = null, string|null $key = null) {
    if (!$text) return;

    $key = $key ?? self::$key;

    if ($key) {
      if (self::$level == 1) {
        $text       = str_split($text);
        $key_split  = str_split($key);
        $key_count  = count($key_split);
        $result_arr = [];
        $i          = 0;

        foreach ($text as $k => $value) {
          $new_chr = chr(ord($value) - $key_split[$i]);
          $result_arr[] = $new_chr;
          $i = ($i + 1) == $key_count ? 0 : $i + 1;
        }

        return join('', $result_arr);
      }
      else if (self::$level == 2) {
        //
      }
    }
    else {
      die("Encrypia error: no key is set.");
    }
  }

}
