<?php
define('DS', DIRECTORY_SEPARATOR);
define('BASE_PATH', dirname(__DIR__) . DS);

function app($c = null) {
  $app = system\App::instance();
  return $c ? $app->$c : $app;
}

//=======[ get domain url ]=========================
function url($path = null) {
  return URL . rtrim(trim($path), '/') . '/';
}

//=======[ get route url by name  ]=================
function route($name, array $args = []) {
  return url(app('route')->getRoute($name, $args));
}

//

require "App.php";
require_once 'core/Startup.php';

$app            = System\App::instance();
$app->request   = System\Request::instance();
$app->route     = System\Route::instance($app->request);
