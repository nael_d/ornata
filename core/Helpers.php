<?php

$base_path    = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['SCRIPT_NAME']);
$base_path    = str_replace("\\", "", $base_path);
$base_path    = !str_ends_with($base_path, '/') ? "{$base_path}/" : $base_path;
$storage_path = $base_path . 'storage/';

foreach ([
  'Essential',
  'Session',
  'Html',
  'Core',
  'Array',
  'Cp',
  'Api',
  'Misc',
  ] as $key => $helper) {
  require_once "core/helpers/{$helper}Helpers.php";
}
