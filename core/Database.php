<?php

class Database {

  public $db_type;
  public $db_con     = null;
  public $db_tables  = [];

  public function __construct() {
    $db = null;

    require_once 'RedBeanX.php';
    require_once 'Engine.php';

    if (!R::testConnection()) {
      // not connected yet
      try {
        if (env('server') != null) {
          $db = R::setup("mysql:host=".env('server').";dbname=".env('database'), env('username'), env('password'));
        }
        else if (is_file(env('database'))) {
          $db = R::setup("sqlite:".env('database'));
        }
      }
      catch (\Throwable $th) {
        //
      }
    }
    else {
      $db = true; // connected
    }

    if ($db == null) {
      if (env('database') == '') {
        // work without db
        $this->db_con = 'offline';
      }
      else {
        // this will propagate through all core and installer.
        $this->db_con = 'error';
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
          errorView("Couldn't connect to the database.", code: 500);
        }
        else {
          die(json_encode(['status' => 'error', 'code' => 500, 'reason' => "Couldn't connect to the database."]));
        }
      }
    }
    else {
      $this->db_con = 'connected';
      R::ext('xdispense', fn($type) => R::getRedBean()->dispense($type));
      $this->db_type = is_file(env('database')) ? 'sqlite3' : 'mysql';
    }
  }

  public function getDatabaseSizes() {
    // RedBeanPHP doesn't directly provide a way to get database sizes.
    // You can use native SQL queries or database-specific methods.

    $databaseSizes = [];

    if (!is_file(env('database'))) {
      $databases = \R::getAll("SHOW DATABASES");
      $excluded_dbs = ['information_schema', 'mysql', 'performance_schema', 'phpmyadmin'];
      foreach ($databases as $database) {
        $dbName = $database['Database'];
        if (in_array($dbName, $excluded_dbs)) continue;
        $sizeResult = \R::getRow("SELECT table_schema AS `database`, SUM(data_length + index_length) AS `size` 
                                  FROM information_schema.TABLES 
                                  WHERE table_schema = ?", [$dbName]);
        $databaseSizes[$dbName] = _filesize($sizeResult['size']);
      }
    }
    else {
      $databaseSizes[env('database')] = _filesize(filesize(env('database')));
    }

    return $databaseSizes;
  }

}