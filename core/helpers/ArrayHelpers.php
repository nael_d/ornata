<?php

function array_is_numeric(string|array $arr) {
  $arr = is_array($arr) ? $arr : json_decode($arr);
  return is_array($arr) && array_keys($arr) === range(0, count($arr) - 1);
}

