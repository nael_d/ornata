<?php

function _filesize(int|float|null $sizeInBytes, string $startUnit = 'b') {
  if (!$sizeInBytes) return;

  $sizeInBytes = (int) $sizeInBytes;
  $units = ['b', 'kb', 'mb', 'gb', 'tb'];
  $unitIndex = array_search(strtolower($startUnit), $units);

  if ($unitIndex === false || $unitIndex >= count($units)) {
    $unitIndex = 0;
  }

  while ($sizeInBytes >= 1024 && $unitIndex < count($units) - 1) {
    $sizeInBytes /= 1024;
    $unitIndex++;
  }

  return round($sizeInBytes, 2) . ' ' . $units[$unitIndex];
}

function platform_detector(string|null $userAgent) {
  if (!$userAgent) return;

  $platforms = [
    'Windows' => 'Windows',
    'Mac' => 'Macintosh|Mac OS X',
    'Android' => 'Android',
    'iOS' => 'iPhone|iPad',
    'Linux' => 'Linux',
  ];

  foreach ($platforms as $platformName => $platformKeywords) {
    if (preg_match("/$platformKeywords/i", $userAgent)) {
      return $platformName;
    }
  }

  return 'Unknown';
}

function convert_googlemap_to_embed(string|null $placeLink) {
  if (!$placeLink) return;

  // Extract latitude, longitude, and place ID from the place link
  preg_match('/@(-?\d+\.\d+),(-?\d+\.\d+)/', $placeLink, $matches);
  $latitude = $matches[1];
  $longitude = $matches[2];

  preg_match('/data=!3m1!4b1!4m6!3m5!1s([^!]+)!8m2!3d(-?\d+\.\d+)!4d(-?\d+\.\d+)/', $placeLink, $matches);
  $placeId = $matches[1];

  // Construct the embedded link
  $embedLink = "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d0!2d{$longitude}!3d{$latitude}!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s{$placeId}!2s!5e0!3m2!1sen!2sen";

  return $embedLink;
}

function server_os() {
  return strtoupper(substr(php_uname('s'), 0, 3)); // returning WIN for windows
}

function server_uptime() {
  return server_os() == 'WIN' ? shell_exec('systeminfo | find "System Boot Time"') : shell_exec('uptime -p');
}

function server_cpuload() {
  return server_os() == 'WIN'
    ? shell_exec('powershell "Get-Counter \"\Processor(_Total)\% Processor Time\" | Select-Object -ExpandProperty CounterSamples | Select-Object -ExpandProperty CookedValue"')
    : sys_getloadavg()[0];
}

function server_ramusage() {
  if (server_os() == 'WIN') {
    $memory = shell_exec('powershell "(Get-Counter \"\Memory\Available MBytes\").CounterSamples.CookedValue"');
  }
  else {
    $free = shell_exec('free -m');
    $free = explode("\n", $free);
    $memory = explode(" ", $free[1]);
    $memory = array_filter($memory);
    $memory = array_merge($memory, []);
    $memory = $memory[2] / $memory[1] * 100; // Get the used memory percentage
  }

  return $memory;
}

function server_diskusage() {
  $diskTotal = disk_total_space('/');
  $diskFree = disk_free_space('/');
  $diskUsed = $diskTotal - $diskFree;

  return [
    'total'         => $diskTotal,
    'used'          => $diskUsed,
    'free'          => $diskFree,
    'used_100'      => ($diskUsed / $diskTotal) * 100,
    'free_100'      => ($diskFree / $diskTotal) * 100,
    'used_to_total' => intval($diskUsed / 1024 / 1024 / 1024) . 'GB/' . intval($diskTotal / 1024 / 1024 / 1024) . 'GB',
  ];
}

function humanize_uptime(string|null $uptime) {
  if (!$uptime) return;

  if (server_os() == 'LIN') {
    // Extract weeks, days, hours, and minutes from the uptime string
    preg_match_all('/(\d+)\s+(weeks?|days?|hours?|minutes?)/', $uptime, $matches, PREG_SET_ORDER);
    
    $totalMinutes = 0;
  
    // Convert weeks, days, hours, and minutes to minutes
    foreach ($matches as $match) {
      $value = (int)$match[1];
      $unit = strtolower($match[2]);
  
      switch ($unit) {
        case 'week':
        case 'weeks':
          $totalMinutes += $value * 7 * 24 * 60;
          break;
        case 'day':
        case 'days':
          $totalMinutes += $value * 24 * 60;
          break;
        case 'hour':
        case 'hours':
          $totalMinutes += $value * 60;
          break;
        case 'minute':
        case 'minutes':
          $totalMinutes += $value;
          break;
      }
    }
  
    $totalDays = floor($totalMinutes / (24 * 60));
    return "{$totalDays} days";
  }
  else {
    // Define possible format variations
    $formats = [
      '!n/j/Y, G:i:s*',
      '!j-M-Y, G:i:s*',
      '!j-M-Y, G:i:s A*',
      '!j-M-Y, H:i:s A*',
      '!j-M-Y, H:i:s*',
    ];

    // Try each format until one succeeds
    $bootDateTime = null;
    foreach ($formats as $format) {
      $bootDateTime = DateTime::createFromFormat($format, trim(str_ireplace("system boot time:", '', $uptime)));
      if ($bootDateTime) break;
    }

    if (!$bootDateTime) return 'Failed to get server uptime details.';

    // Get the current date and time
    $currentDateTime = new DateTime();

    // Calculate the difference in days
    $interval = $currentDateTime->diff($bootDateTime);
    $days = $interval->days;

    return "{$days} days";
  }
}

function parsePingResult(string|null $pingResult) {
  if (!$pingResult) return;

  $lines = explode("\n", trim($pingResult));
  $pingData = [];

  foreach ($lines as $line) {
    if (preg_match('/(\d+) bytes from (.+): icmp_seq=(\d+) ttl=(\d+) time=([\d.]+) ms/', $line, $matches)) {
      $pingData[] = [
        'bytes' => (int)$matches[1],
        'host' => $matches[2],
        'sequence' => (int)$matches[3],
        'ttl' => (int)$matches[4],
        'time' => (float)$matches[5]
      ];
    }
  }

  return $pingData;
}

function avgPing(array|null $res) {
  if (!$res) return;

  $pings = 0;
  $reqs = 0;

  foreach ($res as $key => $value) {
    $pings += $value['time'];
    $reqs++;
  }

  return $pings / $reqs;
}

function check_dns_health(string|null $domain) {
  if (!$domain) return;

  $start = microtime(true);
  $response = dns_get_record($domain, DNS_A);
  $end = microtime(true);

  return $response ? ((round(($end - $start) * 1000, 2)) ?? 0) : 0;
}



