<?php

function api_id_parser(string|int|null $case, string|null $column) {
  if (!$case or !$column) return;

  $patterns = array(
    '/^\d+$/' => "$column = $case",
    '/^\d+(\|\d+)+$/' => "$column IN (" . implode(', ', explode('|', $case)) . ")",
    '/^(\d+)>(\d+)$/' => "$column BETWEEN $1 AND $2",
    '/^(\d+)>(\d+)!(\d+)$/' => "$column BETWEEN $1 AND $2 AND $column != $3",
    '/^(\d+)>(\d+)\|(\d+(\|\d+)*)$/' => "$column BETWEEN $1 AND $2 OR $column IN ($3)",
    '/^>(\d+)$/' => function($matches) use ($column) {
      if ($matches[1] > 0) {
        return "$column > {$matches[1]}";
      }
      else {
        return "Invalid range: Value must be greater than 0.";
      }
    },
    '/^>=1$/' => "$column >= 1",
    '/^<(\d+)$/' => function($matches) use ($column) {
      if ($matches[1] > 0) {
        return "$column < {$matches[1]}";
      }
      else {
        return "Invalid range: Value must be greater than 0.";
      }
    },
    '/^<=(\d+)$/' => function($matches) use ($column) {
      if ($matches[1] > 0) {
        return "$column <= {$matches[1]}";
      }
      else {
        return "Invalid range: Value must be greater than 0.";
      }
    },
    '/^(\d+)>(\d+)!(\d+)>(\d+)$/' => function($matches) use ($column) {
      if ($matches[1] <= $matches[3] && $matches[4] <= $matches[2]) {
        return "$column BETWEEN {$matches[1]} AND {$matches[2]} AND $column NOT BETWEEN {$matches[3]} AND {$matches[4]}";
      }
      else {
        return "Invalid range combination: Excluded range must be entirely within the main range.";
      }
    }
  );

  foreach ($patterns as $pattern => $replacement) {
    if (preg_match($pattern, $case, $matches)) {
      if (is_callable($replacement)) {
        return $replacement($matches);
      }
      else {
        return preg_replace($pattern, $replacement, $case);
      }
    }
  }

  return "Invalid case provided.";
}



