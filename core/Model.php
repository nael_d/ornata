<?php

class Model extends Middleware {

  public bool|null $activeOnly          = true;
  public array $i18n                    = [];
  public array $neutral                 = [];
  private array $sql                    = [];
  private bool $enabledCachingGlobally  = false;
  private int $cacheTimeToLiveGlobally  = 0;
  private int $cacheGracePeriodGlobally = 0;

  public function __construct(public string $table = '', public string $col = '', public string $folder = '') {
    parent::__construct();

    $this->enabledCachingGlobally = env('use_cache_globally') == 'true';
    $this->cacheTimeToLiveGlobally = (int) env('cache_ttl');
    $this->cacheGracePeriodGlobally = (int) env('cache_grace_period');

    $field = null;
    if (!$this->table) { $field = 'table'; }
    else if (!$this->col) { $field = 'col'; }

    if ($field) {
      $this->error(details: "Missing required value for <kbd>{$field}</kbd> property in <kbd>".get_class($this)."::construct</kbd> model.");
    }

    // dpre(array_filter(get_declared_classes(), fn($c) => strstr($c, 'CpModel')));
    // R::getRedBean()->idFieldMap = [$this->table => "id_{$this->col}"];
    // we only set the id only in each method to prevent forced id setting with multiple models when calling
  }

  public function __destruct() {
    $this->sql = [];
  }

  public function set(string $table = '', string $col = '', string $folder = '') {
    $this->table  = $table;
    $this->col    = $col;
    $this->folder = $folder;

    return $this;
  }

  public function with(string $relation_type, string $joined_table, array $relation_map) {
    $cells = $this->sql['cells'] ?? [];
    $joins = $this->sql['joins'] ?? [];

    $cells[]                  = "{$joined_table}.*";
    $joins[$relation_type][]  = [
      'joined_table' => $joined_table, 'join_case' => $relation_map
    ];

    $this->sql['cells'] = $cells;
    $this->sql['joins'] = $joins;

    return $this;
  }

  //

  public function get(int|array|null $id = null, array $sql = [], array $where = [], $cache_ttl = 0, $cache_grace_period = 0) {
    R::getRedBean()->idFieldMap = [$this->table => "id_{$this->col}"];

    $cache_ttl ??= $this->cacheTimeToLiveGlobally;
    $cache_grace_period ??= $this->cacheGracePeriodGlobally;

    if ($this->activeOnly != null) {
      $where["{$this->col}_is_active"] = $where["{$this->col}_is_active"] ?? (int) $this->activeOnly;
    }

    if (is_int($id) and $id > 0) { $where["{$this->table}.id_{$this->col}"] = $id; }
    else if (is_array($id) and count($id) > 0) {
      $where["{$this->table}.id_{$this->col}"] = ['factor' => ' in ', 'value' => "(".join(',', $id).")"];
    }

    if (!array_key_exists("{$this->col}_is_deleted", $where)) { $where["{$this->col}_is_deleted"] = 0; }

    $w = [];
    foreach ($where as $k => $v) {
      $w[$k] = (is_numeric($v) or is_array($v))
        ? $v
        : ($this->use_encrypia == 'true' ? Encrypia::blind($v) : $v);
    }

    $query = [
      'cells'     => ["{$this->table}.*"],
      'table'     => $this->table,
      'where'     => $w,
      'order_by'  => "{$this->table}.id_{$this->col} desc",
    ];

    if (count($this->sql) > 0) {
      $sql = [...$sql, ...$this->sql];
    }

    if (count($sql) > 0) {
      $cell_merged = (isset($sql['cells']) and count($sql['cells']) > 0 ) ? [...$query['cells'], ...$sql['cells']] : $query['cells'];
      $query = [...$query, ...$sql];
      $query['cells'] = $cell_merged;
    }

    $processed_results = [];
    $q = select($query);
    // dpre($q);

    // let's check that this $q query has been cached and not expired yet
    if (!$this->enabledCachingGlobally or !Cache::get($q, gracePeriod: $cache_grace_period)) {
      $results = R::getAll($q);

      if (count($results) > 0) {
        foreach ($results as $i => $result) {
          foreach ($result as $key => $value) {
            if (is_numeric($value) or $value == null) {
              $processed_results[$i][$key] = $value;
            }
            else if (is_array(json_decode($value, true)) and array_reduce(json_decode($value, true), function($a, $b) { return $a && is_int($b); }, true)) {
              $processed_results[$i][$key] = json_decode($value);
            }
            else if (!json_decode($value)) {
              $processed_results[$i][$key] = $this->use_encrypia == 'true' ? Encrypia::unblind($value) : $value;
            }
            else {
              if ($this->use_encrypia == 'true') {
                $processed_results[$i][$key] = (object) array_map(function ($v) {
                  if (is_object($v) or is_array($v)) {
                    return (object) array_map(fn($vv) => Encrypia::unblind($vv), (array) $v);
                  }
                  else {
                    if (!is_int($v) and !is_numeric($v)) { return Encrypia::unblind($v); }
                    else {
                      return $v;
                    }
                  }
                }, (array) json_decode($value));
              }
              else {
                $processed_results[$i][$key] = json_decode($value);
              }
            }
          }
        }

        if ($this->enabledCachingGlobally) {
          Cache::set($q, $processed_results, $cache_ttl, tags: ["cache_table_{$this->table}"]);
        }
      }
    }
    else {
      $processed_results = Cache::get($q, gracePeriod: $cache_grace_period);
    }

    return $processed_results;
    // return R::convertToBean('get', R::getAll($q));
  }

  public function store(array $inputs = [], array $files = [], int|null $id = null) {
    R::getRedBean()->idFieldMap = [$this->table => "id_{$this->col}"];
    set_time_limit(0); // free up the max timeout server time to uplaod files as well

    if (count($inputs) == 0) {
      $this->error(details: "Missing required parameter <kbd>inputs:[...]</kbd> in <kbd>".get_class($this)."</kbd> model.");
    }
    else if (count($this->i18n) == 0 and count($this->neutral) == 0) {
      $this->error(details: "Missing at-least one required property: <kbd>\$i18n = [...]</kbd> or <kbd>\$neutral = [...]</kbd><br />in <kbd>".get_class($this)."</kbd> model.");
    }
    else {
      $inputs = array_map(fn($input) => sanitizer(str_replace("'", '&#39;', $input)), $inputs);
    }

    $i18n     = $this->i18n;
    $neutral  = $this->neutral;
    $item = ($id and $id > 0) ? R::load($this->table, $id) : R::xdispense($this->table);

    if (count($i18n) > 0) {
      foreach ($i18n as $i => $key) {
        ${$key} = [];
        foreach ($this->langs as $lang => $langIso) {
          $v = null;

          if (!isset($inputs["{$this->col}_{$key}_{$langIso}"])) {
            $this->error(details: "i18n key <kbd>{$key}</kbd> is not found in <kbd>\$inputs[]</kbd>, in <kbd>".get_class($this)."</kbd> model.");
          }
          else {
            $v = $inputs["{$this->col}_{$key}_{$langIso}"];
          }

          ${$key}[$langIso] = $this->use_encrypia == 'true' ? Encrypia::blind($v) : $v;
        }
        ${$key} = json_encode(${$key}, JSON_UNESCAPED_UNICODE);
        $item->{"{$this->col}_{$key}"} = ${$key};
      }
    }

    if (count($neutral) > 0) {
      $filtered_keys = array_filter(array_keys($inputs), fn($input) => strpos($input, '-') !== false);
      $obj_handler = [];

      foreach ($neutral as $i => $key) {
        if (is_bool($key)) {
          $neutral[$i] = $i;
          if ($key && $id || !$key && !$id) { continue; } else { $key = $i; }
        }

        if ($key == "id_{$this->col}") {
          $v = $inputs[$key];
        }
        else if (is_numeric($inputs["{$this->col}_{$key}"])) {
          $v = intval($inputs["{$this->col}_{$key}"]);
        }
        else if (
          (
            gettype($inputs["{$this->col}_{$key}"]) == 'string' and
            in_array(gettype(json_decode($inputs["{$this->col}_{$key}"])), ['object', 'array'])
          ) or 
          (in_array(gettype($inputs["{$this->col}_{$key}"]), ['object', 'array']))
        ) {
          $v = gettype($inputs["{$this->col}_{$key}"]) == 'string'
            ? (array) json_decode($inputs["{$this->col}_{$key}"])
            : $inputs["{$this->col}_{$key}"];
          if ($this->use_encrypia == 'true') {
            $temp = [];
            foreach ($v as $k => $v2) {
              if (is_numeric($v2)) { $temp[$k] = $v2; }
              else if (is_string($v2)) { $temp[$k] = Encrypia::blind($v2); }
              else {
                $temp[$k] = array_map(fn($vv2) => Encrypia::blind($vv2), (array) $v2);
              }
            }
            $v = json_encode($temp);
          }
          else {
            $v = json_encode($v);
          }
        }
        else {
          if (count($filtered_keys) > 0) {
            foreach ($filtered_keys as $fk => $fv) {
              $exploded_fv = explode('-', $fv);
              // "{$this->col}_{$key}"-{objKey}
              // we used the neutral key to store an object
              if ($exploded_fv[0] == "{$this->col}_{$key}") {
                $obj_handler["{$this->col}_{$key}"][$exploded_fv[1]] = $this->use_encrypia == 'true'
                  ? Encrypia::blind($inputs[$fv])
                  : $inputs[$fv];
                unset($filtered_keys[$fk]);
              }
            }

            $v = json_encode($obj_handler["{$this->col}_{$key}"], JSON_UNESCAPED_UNICODE);
          }
          else {
            if (!isset($inputs["{$this->col}_{$key}"])) {
              $this->error(details: "Neutral key <kbd>{$key}</kbd> is not found in <kbd>\$inputs[]</kbd>, in <kbd>".get_class($this)."</kbd> model.");
            }
            else {
              $v = ($this->use_encrypia == 'true' and !empty($inputs["{$this->col}_{$key}"]))
                ? Encrypia::blind($inputs["{$this->col}_{$key}"])
                : $inputs["{$this->col}_{$key}"];
            }
          }
        }

        if ($key == "id_{$this->col}") {
          $item->{"id_{$this->col}"} = $v;
        }
        else {
          $item->{"{$this->col}_{$key}"} = $v;
        }
      }
    }

    /**
     * Looping through $files.
     * IMPORTANT! the keys that we're going to discover and store them MUST be named EXACTLY in the realated table.
     */
    foreach ($files as $key => $file) {
      if (!is_array($file['error'])) {
        // it's a single file to be uploaded
        if ($file['error'] == 0) {
          $upload = $this->upload(
            $file,
            $this->folder,
            explode('/', $file['type'])[0], rand_str(8)
          );
          $item->$key = $this->use_encrypia == 'true' ? Encrypia::blind($upload) : $upload;
        }
      }
      else {
        // it's a multiple files to be uploaded
        $resorted_files = [];
        $files_names    = [];

        foreach ($file as $k => $f) {
          foreach ($f as $i => $v) {
            if ($file['error'][$i] != 0) { continue 2; }
            // $v ? ($resorted_files[$i][$k] = $v) : null;
            $resorted_files[$i][$k] = $v;
          }
        }

        if (count($resorted_files) > 0) {
          foreach ($resorted_files as $k2 => $resorted_file) {
            $upload = $this->upload(
              $resorted_file,
              $this->folder,
              explode('/', $resorted_file['type'])[0],
              rand_str(8)
            );
            $files_names[] = $this->use_encrypia == 'true' ? Encrypia::blind($upload) : $upload;
          }
        }

        $files_final = [];
        if ($item->$key != '' and is_array(json_decode($item->$key))) {
          $files_final = [...$files_names, ... (array) json_decode($item->$key)];
        }
        else {
          $files_final = $files_names;
        }

        $item->$key = json_encode($files_final, JSON_UNESCAPED_UNICODE);
      }

      unset($files[$key]); // this removes this item to prevent unexpected repeated looping
    }

    if ($id == null) {
      // storing new item, no need to update those columns because it affects database type problem.
      $item->{"{$this->col}_is_active"}     = (count($neutral) > 0 and in_array('is_active', $neutral)) ? $inputs["{$this->col}_is_active"] : 1;
      $item->{"{$this->col}_is_deleted"}    = (count($neutral) > 0 and in_array('is_deleted', $neutral)) ? $inputs["{$this->col}_is_deleted"] : 0;
      $item->{"{$this->col}_date_created"}  = $this->use_encrypia == 'true' ? Encrypia::blind(date('Y-m-d H:i:s')) : date('Y-m-d H:i:s');
    }
    else {
      $item->{"{$this->col}_date_updated"} = $this->use_encrypia == 'true' ? Encrypia::blind(date('Y-m-d H:i:s')) : date('Y-m-d H:i:s');
    }

    Cache::clearByTag(["cache_table_{$this->table}"]);
    return R::store($item);
  }

  public function delete(array $list = []) {
    R::getRedBean()->idFieldMap = [$this->table => "id_{$this->col}"];

    if (count($list) == 0) {
      $this->error(details: "Missing required value for <kbd>delete</kbd> method in <kbd>".get_class($this)."</kbd> model.");
    }
    else {
      R::exec("update {$this->table} set {$this->col}_is_deleted=1 where id_{$this->col} in (" . implode(',', $list) . ")");
      if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) { return true; }
      else { echo json_encode(['status' => 'ok']); }
    }
  }

}