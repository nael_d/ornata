<?php

use Corviz\Crow\Crow as View;

class Controller extends Middleware {

  protected $DS = DIRECTORY_SEPARATOR;

  protected const View = View::class;

  public $controller          = null;
  public $views_root_path     = 'web'; // to determine (views/web/ or views/cp/)
  public $current_lang        = null;
  public $current_path        = null;
  public $current_path_full   = null;
  public $current_path_array  = null;
  public $current_query_uri   = null;
  public $extra_params        = []; // this is used in ControllerExtended
  public $sublayout           = [];

  public function __construct() {
    parent::__construct();
    // $this->load_error_core_layout = $this->views_root_path == 'web';
    /**
     * this property is to control loading core layout for error page.
     * in Controller, sometimes we only need to load the error page without its core layout.
     * so, this property handles it as well in error() here below.
     */

    View::setDefaultPath("views{$this->DS}{$this->views_root_path}");
    View::setExtension('');
    View::disableCodeMinifying();
    self::View::setComponentsNamespace($this->views_root_path == 'web' ? 'WebComponent' : 'CpComponent');
    View::$global = $this; // to globalize $this

    if ($this->use_encrypia == 'true' and !$this->encrypia_key) {
      $this->error(details: "No key passed for Encrypia in <kbd>.env</kbd>");
    }

    $this->controller = strtolower(get_class($this));

    if (strpos($this->controller, '\\')) {
      $this->controller = explode('\\', $this->controller)[1]; // to remove any namespaces in the class
    }

    $this->current_query_uri  = app('request')->server['REDIRECT_QUERY_STRING'];
    $this->current_path       = substr(app('request')->path, 0, -1) . ($this->current_query_uri != '' ? "?{$this->current_query_uri}" : '');
    $this->current_path_array = array_slice(explode('/', $this->current_path), 1);

    if (count($this->current_path_array) == 0) {
      $this->current_path_array[] = '/';
    }

    $this->current_path_full  = path($this->current_path);

    session('core', [
      'current_path'        => $this->current_path,
      'current_path_array'  => $this->current_path_array,
      'current_path_full'   => $this->current_path_full,
      'current_query_uri'   => $this->current_query_uri,
    ]);

    if ($this->current_path_array[0] != 'cronjobs') {
      // cronjobs shouldn't being monitored because it's used by the server.
      if ($this->current_path_array[0] == 'cp') {
        $this->platform = 'cp';

        if (__REQUEST__ == 'get') {
          $this->current_lang = $this->default_lang_cp;

          session('cp', [
            'controller'          => $this->controller,
            'current_path'        => $this->current_path,
            'current_path_array'  => $this->current_path_array,
            'current_path_full'   => $this->current_path_full,
            'default_lang'        => $this->default_lang_cp,
          ]);
        }
      }
      else if ($this->current_path_array[0] == 'api') {
        $this->platform = 'api';
        //
      }
      else {
        if (__REQUEST__ == 'get') {
          $this->current_lang = session('web', 'current_lang') ?? $this->default_lang_web;
          $this->platform = 'web';

          session('web', [
            'controller'          => $this->controller,
            'current_path'        => $this->current_path,
            'current_path_array'  => $this->current_path_array,
            'current_path_full'   => $this->current_path_full,
            'default_lang'        => $this->default_lang_web,
            'current_lang'        => $this->current_lang,
          ]);
        }
      }
    }
  }

  public function load_model(array|string $models = '') {
    $debug  = debug_backtrace();

    foreach ($debug as $key => $value) {
      if (in_array($value['class'], ['Controller'])) { continue; }
      else {
        $debug = $value;
        break;
      }
    }

    $class_and_method = "{$debug['class']}::{$debug['function']}";

    if (is_string($models)) {
      if ($models == '') {
        $this->error(details: "No model passed in <kbd>{$class_and_method}</kbd>");
      }
      else {
        $model_exploded = explode('\\', $models);
        $model_folder = strpos($models, '\\') !== false
          ? strtolower(str_replace('Model', '', $model_exploded[0]))
          : $this->views_root_path;
        /**
         * we used this way to let the developer load any model that he needs even if it's in another layer.
         * for example: in web layer, maybe he needs to load a cp layer model.
         * I don't know why could he need it, but it's prepared anyway.
         */
        $model = ".{$this->DS}models{$this->DS}{$model_folder}{$this->DS}{$model_exploded[1]}";
        $model .= !str_ends_with($model, '.php') ? '.php' : '';
        $model = str_replace("{$this->DS}{$this->DS}", $this->DS, $model);

        if (!file_exists($model)) {
          $this->error(details: "Trying to load not-exist <kbd>".basename($model)."</kbd> model in <kbd>$class_and_method</kbd>");
        }
        else {
          require_once $model;
          return $this;
        }
      }
    }
    else {
      foreach ($models as $model) {
        $this->load_model($model);
      }
    }
  }

}
