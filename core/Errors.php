<?php

class Errors extends ControllerExtended {

  private $statuses = [];

  public function __construct() {
    parent::__construct();

    $this->views_root_path = '';
    $this->statuses = [
      404 => [
        'title'     => "Not found",
        'details'   => "Target <kbd>".($this->current_path != '/' ? $this->current_path : '')."</kbd> is not found",
        'show_btn'  => true,
      ],
      422 => [
        'title'     => "Application Error",
        'details'   => null,
        'show_btn'  => false,
      ],
      500 => [
        'title'     => "Server Error",
        'details'   => null,
        'show_btn'  => false,
      ],
      503 => [
        'title'     => "Service Unavailable",
        'details'   => null,
        'show_btn'  => false,
      ],
    ];
  }

  public function __destruct() {
    die();
  }

  public function website($code, $details, $force_error = false) {
    $this->error(
      code: $code,
      title: $this->statuses[$code]['title'],
      details: $this->statuses[$code]['details'] ?? $details,
      show_btn: $this->statuses[$code]['show_btn'],
      force_error: $force_error,
    );
  }

  public function api($code, $details) {
    http_response_code($code);
    echo json_encode([
      'status'      => 'error',
      'status_text' => $this->statuses[$code]['details'] ?? $details,
      'code'        => $code,
      'reason'      => "The route {$this->current_path} is not found."
    ]);
    die();
  }

  public function cp($code, $details, $force_error = false) {
    $this->error(
      code: $code,
      title: $this->statuses[$code]['title'],
      details: $this->statuses[$code]['details'] ?? $details,
      show_btn: $this->statuses[$code]['show_btn'],
      force_error: $force_error,
    );
  }

}