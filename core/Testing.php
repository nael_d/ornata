<?php

namespace Testing;

class Testing {

  public static $tests = [];

  public static function declare($testId, $testName, $function, $parameters, $expected_result) {
    self::$tests[$testId] = [
      'name'            => $testName,
      'function'        => $function,
      'parameters'      => $parameters,
      'expected_result' => $expected_result
    ];
  }

  public static function run($testId) {
    $result = [];

    if (isset(self::$tests[$testId])) {
      $microtime_start    = microtime(true);
      $test               = self::$tests[$testId];
      $actual_result      = !is_callable($test['function']) ? call_user_func_array($test['function'], $test['parameters']) : $test['function'](...$test['parameters']);
      $status             = $actual_result == $test['expected_result'] ? '✅ PASSED' : '❌ FAILED';
      $functionName       = is_callable($test['function']) ? (is_string($test['function']) ? $test['function'] : 'λ Function') : 'Invalid Function';
      $test['parameters'] = json_encode($test['parameters']);
      $microtime_end      = microtime(true);

      if (in_array(gettype($test['expected_result']), ['array', 'object'])) {
        $test['expected_result'] = json_encode($test['expected_result']);
        $actual_result           = json_encode($actual_result);
      }

      $result = [
        'ID'          => $testId,
        'Name'        => $test['name'],
        'Function'    => $functionName,
        'Parameters'  => strlen($test['parameters'])      < 20 ? $test['parameters']      : truncate_text($test['parameters'], 15, truncate_middle: true),
        'Expected'    => strlen($test['expected_result']) < 30 ? $test['expected_result'] : truncate_text($test['expected_result'], 15, truncate_middle: true),
        'Actual'      => strlen($actual_result)           < 30 ? $actual_result           : truncate_text($actual_result, 15, truncate_middle: true),
        'Status'      => $status,
        'Period (μs)' => round((($microtime_end - $microtime_start) * 1_000_000), 5),
      ];
    }
    else {
      $result = [
        'ID'          => $testId,
        'Name'        => null,
        'Function'    => null,
        'Parameters'  => null,
        'Expected'    => null,
        'Actual'      => null,
        'Status'      => "😕 MISSING",
        "Period"      => null,
      ];
    }

    return $result;
  }

}
