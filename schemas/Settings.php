<?php

namespace Schema;

class Settings extends Schema implements SchemaBlueprint {

  private $settings = [];

  public function __construct() {
    parent::__construct();

    $this->settings = [
      'gauth'             => 1,
      'dark-mode'         => 0,
      'website-status'    => "{\"message\":{},\"status\":0,\"activation_date\":null}",
      'privacy-policy'    => "{}",
      'terms-of-service'  => "{}",
      'about-us'          => "{}",
      'contact-us'        => "{}",
    ];

    if ($this->use_encrypia == 'true') {
      // blind the setting keys.
      $this->settings = array_combine(
        array_map(fn($key) => \Encrypia::blind($key), array_keys($this->settings)),
        $this->settings
      );
    }
  }

  public function install() {
    $this->design();
    $this->write();
  }

  public function design() {
    $this
      ->table('settings')
      ->prefix('setting')
      ->integer('id_*')->autoIncrement()->unsigned()->primaryKey()
      ->string('*_key')
      ->longtext('*_value')
      ->string('*_image')->allowNull()
      ->string('*_images', 1000)->allowNull()
      // ->string('*_date_created') // we don't need this field for settings because it's not necessary to track the creation date of a setting.
      ->string('*_date_updated')->allowNull()
      ->tinyint('*_is_active')->defaultValue(1)->unsigned()
      ->tinyint('*_is_deleted')->defaultValue(0)->unsigned()
      ->build()
    ;
  }

  public function write() {
    $writer = $this->table('settings')->prefix('setting');
    foreach ($this->settings as $key => $value) {
      $writer->insert(['*_key' => $key, '*_value' => $value]);
    }
  }

}