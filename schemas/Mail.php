<?php

namespace Schema;

class Mail extends Schema implements SchemaBlueprint {

  public function __construct() {
    parent::__construct();
  }

  public function install() {
    $this->design();
  }

  public function design() {
    $this
      ->table("mail")
      ->prefix('mail')
      ->integer('id_*')->autoIncrement()->primaryKey()->unsigned()
      ->string('*_title')
      ->string('*_sender')
      ->string('*_sender_email')
      ->string('*_sender_phone')
      ->longtext('*_text')
      ->string('*_date_created')
      ->tinyint('*_is_read')->defaultValue(0)
      ->tinyint('*_is_active')->unsigned()->defaultValue(1)
      ->tinyint('*_is_deleted')->unsigned()->defaultValue(0)
      ->build()
    ;
  }

}