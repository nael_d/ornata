<?php

/**
 * To call class in objective way: use ['Class', 'Method']
 * To call class in static way: use 'Class@index'
 */

$route->group('/cronjobs', function () {
  $this->get('/wipe_cache',                 ['Cronjobs\Core', 'wipe_cache']);
  $this->get('/sendmails',                  ['Cronjobs\Core', 'maillist_trigger']);
  $this->get('/website_activator',          ['Cronjobs\Core', 'website_activator']);
  $this->get('/backup_database',            ['Cronjobs\Core', 'backup_database']);
  $this->get('/database_backups_archiver',  ['Cronjobs\Core', 'database_backups_archiver']);
  $this->get('/test',                       ['Cronjobs\Core', 'test']);
});