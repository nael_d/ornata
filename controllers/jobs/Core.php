<?php

/**
 * LogLevel::EMERGENCY => 0,
 * LogLevel::ALERT     => 1,
 * LogLevel::CRITICAL  => 2,
 * LogLevel::ERROR     => 3,
 * LogLevel::WARNING   => 4,
 * LogLevel::NOTICE    => 5,
 * LogLevel::INFO      => 6,
 * LogLevel::DEBUG     => 7
 */

namespace Cronjobs;

class Core extends Cronjobs {

  public function __construct($init = true) {
    $this->logging_each = true; // to prevent logging this task only
    parent::__construct($init);
    $this->set_description("Contains basic built-in tasks in Ornata.");
  }

  public function wipe_cache() {
    $i = 0;

    foreach ([...array_diff(scandir('.cache'), ['.', '..', 'ignored'])] as $key => $cache) {
      unlink(".cache/{$cache}");
      $i++;
    }

    if ($i > 0) {
      $this->logger("Cache folder wiped ({$i} item" . ($i == 1 ? '' : 's') . ").", "info");
      echo "Cache folder wiped ({$i} item" . ($i == 1 ? '' : 's') . ").";
    }
  }

  public function backup_database() {
    if (!is_dir('.backups/databases')) {
      mkdir('.backups/databases', 0755, true);
    }

    $backup = $this->backup();

    switch ($backup['status']) {
      case 'ok':
        $this->logger("Database ({$backup['name']}) has been successfully backed up.", 'info');
        echo "Database ({$backup['name']}) has been successfully backed up.";
        break;
      case 'error':
        switch ($backup['reason']) {
          case 2:
            $this->logger("Error #2: Command 'mysqldump' is not found. Ensure that it's installed or consult your system administrator.", 'error');
            break;
          case 126:
            $this->logger("Error #126: Permission problem or command not executable. Check file permissions.", 'error');
            break;
          case 128:
            $this->logger("Error #128: Invalid argument. Please review your input and try again.", 'error');
            break;
          case 130:
            $this->logger("Error #130: Command terminated. Please avoid interruptions during execution.", 'error');
            break;
          case 255:
            $this->logger("Error #128: Database access denied. Please check your credentials and ensure you have permission to export the data.", 'error');
            break;
          case 1:
          default:
            $this->logger("Error #1: General failure. Please try again later or contact your developer.", 'error');
            break;
        }
      break;
    }
  }

  public function database_backups_archiver() {
    $filename               = date('Y-m-d_H-i-s');
    $numberOfSelectedFiles  = 5; // abandoned
    $allowedExtensions      = ['sqlite3', 'sql'];
    $folderToBackup         = '.backups/databases';
    $archiveOutputName      = ".backups/archives/databases/{$filename}.zip";

    // Create a new ZipArchive instance
    $zip = new \ZipArchive();

    // Open the archive file
    if (!is_dir('.backups/archives/databases')) mkdir('.backups/archives/databases', 0755, true);
    if ($zip->open($archiveOutputName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE | \ZipArchive::CHECKCONS)) {
      // Set the compression level to maximum (9)
      $zip->setCompressionIndex(0, \ZipArchive::CM_DEFAULT, 9);

      // List all files in the folder
      $filesInFolder = scandir($folderToBackup);

      // Filter files based on allowed extensions
      $selectedFiles = array_filter($filesInFolder, function ($file) use ($allowedExtensions) {
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        return in_array($extension, $allowedExtensions);
      });

      // Select a specified number of files
      // $selectedFiles = array_slice($selectedFiles, 0, $numberOfSelectedFiles);

      // Add selected files to the archive
      foreach ($selectedFiles as $file) {
        $filePath = $folderToBackup . '/' . $file;
        $zip->addFile($filePath, $file);
        // unlink($filePath); // to remove the file after being compressed
      }

      // Close the archive
      $zip->close();

      // Delete backups after being archived
      foreach ($selectedFiles as $file) {
        $filePath = $folderToBackup . '/' . $file;
        unlink($filePath);
      }

      // Logging the cronjob task
      $this->logger("Archive (.backups/archives/databases/{$filename}.zip) has been created.", 'info');
      echo "Archive (.backups/archives/databases/{$filename}.zip) has been created.";
    }
    else {
      $this->logger("Failed to archive database backups at this run, unable to open 'zip' file.", 'error');
    }
  }

  public function maillist_trigger() {
    $this->load_model("CpModel\Maillist");
    $maillist = new \CpModel\Maillist;
    $now = strtotime('now');
    $subs = array_map(fn($m) => $m['maillist_sub_email'], $maillist->get_subs());

    // let's get all scheduled emails
    $scheduled = $maillist->get_mails(0);
    
    if (count($scheduled) > 0) {
      // there are scheduled emails. let's check their date and compare them with current date.
      echo "Scheduled mails detected. Checking for their date ...\n";
      $i = 0;

      foreach ($scheduled as $key => $sch) {
        if ($now >= $sch['maillist_mail_date_scheduled']) {
          $i++;

          if (!__LOCALHOST__) {
            mail('', $sch['maillist_mail_title'], $sch['maillist_mail_message'], [
              'From' => "no-reply@{$_SERVER['HTTP_HOST']}",
              'X-Mailer' => 'PHP/' . phpversion(),
              'Bcc' => implode(', ', $subs),
            ]);
          }

          $maillist->mark_mail_as_sent($sch['id_maillist_mail']);
          $this->logger("Successfully sending mail (#{$sch['id_maillist_mail']}): `{$sch['maillist_mail_title']}`.", \Psr\Log\LogLevel::INFO);
          echo "Successfully sending mail (#{$sch['id_maillist_mail']}): `{$sch['maillist_mail_title']}`.\n";
        }
      }

      echo $i == 0 ? "No scheduled emails to send." : "Total scheduled emails sent: ({$i})";
    }
    else {
      echo "No scheduled emails to send.";
    }
  }

  public function website_activator() {
    $this->load_model('CpModel\Settings');
    $model            = new \CpModel\Settings;
    $setting          = $model->preference_get_value('website-status');
    $activation_date  = value_json($setting, 'activation_date');
    $status           = value_json($setting, 'status');
    $now              = strtotime("now");
    $sch              = strtotime(str_replace('/', '-', $activation_date));

    if ($status == 0) {
      if ($sch and $sch <= $now) {
        // scheduled date is invalid: set into the past from now. so, we'll activate the website and ignore it.
        $status = 1;
      }

      if ($status == 1) {
        $setting->status = 1;
        $model->preference_set('website-status', json_encode($setting)) ? true : false;
        $this->logger("Website activated.", \Psr\Log\LogLevel::INFO);
        echo "Website activated.";
      }
    }
  }

  public function test() {
    echo 'test';
  }

}