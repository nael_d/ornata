<?php

/**
 * LogLevel::EMERGENCY => 0,
 * LogLevel::ALERT     => 1,
 * LogLevel::CRITICAL  => 2,
 * LogLevel::ERROR     => 3,
 * LogLevel::WARNING   => 4,
 * LogLevel::NOTICE    => 5,
 * LogLevel::INFO      => 6,
 * LogLevel::DEBUG     => 7
 */

namespace Cronjobs;

class Blank extends Cronjobs {

  public function __construct($init = true) {
    // $this->logging_each = false; // to prevent logging this task only
    parent::__construct($init);
    $this->set_description("");
  }

  public function test() {
    $this->logger("The result of this is ok", 'info');
  }

  public function info() {
    // echo $this->key;
  }
  
}