<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

// namespace WebController|CpController|ApiController
namespace CpController;

class Settings Extends CP {

  private $model        = 'CpModel\Settings';
  private $redirectTo   = 'cp/settings';
  private $title        = "Settings";
  private $title_single = "setting";
  private $title_column = 'setting';

  public function __construct() {
    parent::__construct();
    $this->model = new $this->model;
  }

  public function index() {
    self::View::render('settings/index', [
      'title'     => $this->title,
      'gauth'     => $this->model->preference_get('gauth')['setting_value'],
      // 'darkmode'  => $this->model->preference_get('dark-mode')['setting_value'],
      'darkmode'  => $this->cp_user['cp_user_is_darkmode'],
    ]);
  }

  public function gauth() {
    // if gauth is enabled, disable it; and vice versa.
    $this->model->preference_set('gauth', (int) !$this->model->preference_get('gauth')['setting_value']);
    redirect($this->redirectTo);
  }

  public function darkmode() {
    // if dark-mode is enabled, disable it; and vice versa.
    $auth = new \CpModel\Auth();
    $auth->neutral = ['is_darkmode'];
    $auth->store(
      ['cp_user_is_darkmode' => (int) !$this->cp_user['cp_user_is_darkmode']],
      id: session('cp', 'id_cp_user')
    );
    // $this->model->preference_set('dark-mode', (int) !$this->model->preference_get('dark-mode')['setting_value']);
    redirect($this->redirectTo);
  }

  //

}