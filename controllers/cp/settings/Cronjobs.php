<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

namespace CpController;

class Cronjobs Extends Cp {

  private $title    = "Cronjobs";
  private $cronjobs = [];

  public function __construct() {
    parent::__construct();

    $cronjobs = array_diff(
      array_map(
        fn($c) => pathinfo($c, PATHINFO_FILENAME),
        glob('controllers/jobs/*.php')
      ), ['Blank']
    );

    foreach ($cronjobs as $key => $cronjob) {
      $class = "Cronjobs\\{$cronjob}";
      $refClass = new \ReflectionClass($class);
      $instance = new $class(false);
      $this->cronjobs[$cronjob]['description'] = $instance->get_description();

      foreach ($refClass->getMethods() as $i => $method) {
        if ($method->name == '__construct') continue;
        if ($method->class == $class) {
          $this->cronjobs[$cronjob]['methods'][] = $method->name;
        }
      }
    }
  }

  public function index() {
    // list all cronjobs classes
    self::View::render('settings/cronjobs/index', [
      'title' => $this->title,
      'items' => $this->cronjobs,
    ]);
  }

  public function get($cronjob) {
    // list all this cronjob methods
    if (!$this->cronjobs[$cronjob]) redirect('cp/settings/cronjobs');
    self::View::render('settings/cronjobs/view', [
      'title'   => "Tasks of `{$cronjob}` cronjob",
      'items'   => $this->cronjobs[$cronjob]['methods'],
      'cronjob' => $cronjob,
    ]);
  }

}