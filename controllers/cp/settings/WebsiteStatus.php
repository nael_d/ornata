<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

namespace CpController;

class WebsiteStatus Extends Cp {

  private $model;
  private $redirectTo   = 'cp/settings/website-status';
  private $title        = "Website status";

  public function __construct() {
    parent::__construct();
    $this->load_model('CpModel\Settings');
    $this->model = new \CpModel\Settings;
  }

  public function index() {
    self::View::render('settings/website-status/view', [
      'title' => $this->title,
      'item'  => $this->model->preference_get('website-status'),
    ]);
  }

  public function store() {
    $this->model->folder = 'website-status';
    $this->model->store(inputs: $_POST, files: $_FILES, id: 'website-status');
    redirect($this->redirectTo);
  }

}