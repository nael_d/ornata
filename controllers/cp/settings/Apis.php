<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

// namespace WebController|CpController|ApiController
namespace CpController;

class Apis Extends CP {

  private $model        = 'CpModel\Apis';
  private $redirectTo   = 'cp/settings/apis';
  private $title        = "APIs";
  private $title_single = "API";
  private $title_column = 'api_belongs_to';

  public function __construct() {
    parent::__construct();
    $this->load_model([$this->model]);
    $this->model = new $this->model;
  }

  public function get(int|null $id = null) {
    if ($id > 0) {
      $item = $this->model->get($id);
      if (count($item) == 0) redirect($this->redirectTo);

      self::View::render('settings/apis/view', [
        'title'   => "Edit {$this->title}",
        'item'    => current($item),
        'tables'  => $this->db_tables,
      ]);
    }
    else {
      self::View::render('settings/apis/index', [
        'title' => $this->title,
        'items' => $this->model->get(),
      ]);
    }
  }

  public function create() {
    self::View::render('settings/apis/view', [
      'title'   => "Create new {$this->title}",
      'api_key' => rand_str(32),
      'tables'  => $this->db_tables,
    ]);
  }

  public function store(int|null $id = null) {
    $inputs = $_POST;

    $this->model->store($inputs, [], $id);

    redirect("{$this->redirectTo}/{$id}");
  }

}
