<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

namespace CpController;

class AboutUs Extends Cp {

  private $model;
  private $redirectTo   = 'cp/about-us';
  private $title        = "About us";

  public function __construct() {
    parent::__construct();
    $this->load_model('CpModel\Settings');
    $this->model = new \CpModel\Settings;
    $this->model->folder = 'about-us';
  }

  public function index() {
    self::View::render('about-us/view', [
      'title' => $this->title,
      'item'  => $this->model->preference_get('about-us'),
    ]);
  }

  public function store() {
    $this->model->store(inputs: $_POST, files: $_FILES, id: 'about-us');
    redirect($this->redirectTo);
  }

}