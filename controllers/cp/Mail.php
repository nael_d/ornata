<?php

namespace CpController;

class Mail extends CP {

  private $model;
  private $counters = [];
  private $link = 'cp/mail';
  private $layout = 'mail';
  private $title = 'mail';

  public function __construct() {
    parent::__construct();

    $this->model = new \CpModel\Mail;

    self::View::setViewbag([
      'counters'  =>  [
        'inbox'   => count($this->model->get_mails()),
        'unread'  => count($this->model->get_mails('unread')),
        'trash'   => count($this->model->get_mails('trash')),
      ],
    ]);
  }

  public function index() {
    self::View::render('mail/index', [
      'title' => 'Mail Inbox',
      'mails' => $this->model->get_mails(),
    ]);
  }

  public function unread() {
    self::View::render('mail/index', [
      'title' => 'Mail Unread',
      'mails' => $this->model->get_mails('unread'),
    ]);
  }

  public function trash() {
    self::View::render('mail/index', [
      'title' => 'Mail Trash',
      'mails' => $this->model->get_mails('trash'),
    ]);
  }

  public function read(int $id_mail) {
    // nulling `activeOnly` to ignore `is_active` condition at all
    $this->model->activeOnly = null;
    $mail = $this->model->get(where: ['id_mail' => $id_mail]);
    
    if (count($mail) > 0) {
      $this->model->set_mail_read($id_mail);
      $mail = $mail[0];

      self::View::render('mail/read', [
        'title' => "Mail from {$mail['mail_sender']}",
        'mail'  => $mail,
      ]);
    }
    else {
      redirect('cp/mail');
    }
  }

  public function delete_one($id) {
    $this->model->toggle_mail($id, 'delete');
    redirect('cp/mail');
  }
  public function restore_one($id) {
    $this->model->toggle_mail($id, 'restore');
    redirect('cp/mail');
  }

}
