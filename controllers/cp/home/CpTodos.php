<?php

namespace CpController;

class CpTodos extends CP {

  private $model;

  public function __construct() {
    parent::__construct();

    $this->load_model(["CpModel\CpTodos"]);
    $this->model = new \CpModel\CpTodos;
  }

  public function todos_store() {
    $todo = sanitizer($_POST['todo']);
    $new_todo = $this->model->store([
      'cp_todo_id_user'   => $this->cp_user['id_cp_user'],
      'cp_todo_title'     => $todo,
      'cp_todo_is_active' => 0,
    ]);
    echo json_encode(current($this->model->get($new_todo)));
  }

  public function todos_activity() {
    $id_todo = sanitizer($_POST['id_cp_todo']);
    $this->model->neutral = ['is_active'];
    $activity = (int) current($this->model->get($id_todo))['cp_todo_is_active'] == 1 ? 0 : 1;
    $this->model->store(inputs: ['cp_todo_is_active' => $activity], id: $id_todo);
    echo 'ok';
  }

  public function todos_delete() {
    $id_todo = sanitizer($_POST['id_cp_todo']);
    $this->model->delete([$id_todo]);
  }

}