<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

namespace CpController;

class Maillist Extends CP {

  private $model        = null;
  private $redirectTo   = 'cp/maillist';
  private $title        = "Mailing list";
  private $title_single = "";
  private $title_column = '';

  public function __construct() {
    parent::__construct();
    $this->model = new \CpModel\Maillist;
  }

  public function index() {
    self::View::render('maillist/index', [
      'title'                 => $this->title,
      'subs_count'            => $this->model->get_subs_count(),
      'sent_mails_count'      => count($this->model->get_mails(1)),
      'scheduled_mails_count' => count($this->model->get_mails(0)),
    ]);
  }

  public function create() {
    self::View::render('maillist/create', [
      'title' => 'Create mail',
    ]);
  }

  public function store($id = null) {
    $this->model->save_mail($_POST, $id);
    redirect("{$this->redirectTo}/" . ($id ?? ''));
  }

  public function list() {
    $type = $this->current_path_array[2];

    self::View::render('maillist/list', [
      'title' => ($type == 'list' ? 'Sent' : 'Scheduled') . " mails",
      'ptype' => $type,
      'items' => $this->model->get_mails(match ($type) {
        'scheduled' => 0,
        'sent'      => 1,
        'deleted'   => 2,
      }),
    ]);
  }

  public function review($id) {
    $mail = $this->model->get_mails(id: $id);
    if (count($mail) > 0) {
      $mail = $mail[0];
      self::View::render('maillist/create', [
        'title' => "Review maillist mail: ({$mail['maillist_mail_title']})",
        'item'  => $mail,
      ]);
    }
    else {
      redirect($this->redirectTo);
    }
  }

}