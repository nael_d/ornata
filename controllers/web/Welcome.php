<?php

namespace WebController;

class Welcome extends \ControllerExtended {

  public function __construct() {
    parent::__construct();
  }

  public function index() {
    self::View::render("welcome/index", ['base_path' => $this->base_path]);
  }

}
