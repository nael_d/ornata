<div class="layout-error">
  <div class="error-page container">
    <div class="title"><?=$code?></div>
    <?php if (isset($title) and $title): ?><div class="hint"><?=$title?></div><?php endif; ?>
    <?php if (isset($details) and $details): ?><h4 style='margin: 15px 0'><?=$details?></h4><?php endif; ?>
    <?php if (isset($show_btn) and $show_btn): ?>
      <a class="btn" href="<?=path()?>">Return to home page</a>
      <div class="text-sm">
        <a onclick="history.back()" style='font-size: 14px; cursor: pointer; color: magenta;'>Go back</a>
      </div>
    <?php endif; ?>
  </div>
</div>