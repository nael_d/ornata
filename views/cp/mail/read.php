@extends('mail/layout')

@section('mail-content')
  <div class="card card-primary card-outline">
    <div class="card-header">
      <h3 class="card-title">Read Mail</h3>
    </div>

    <div class="card-body p-0">
      <div class="mailbox-read-info">
        <h5 class="h5">{{ $mail['mail_title'] }}</h5>
        <p>
          <small>From: {{ $mail['mail_sender'] }} ({{ $mail['mail_sender_email'] }}) ({{ $mail['mail_sender_phone'] ?? 'No mobile number' }})</small>
          <span class="mailbox-read-time float-right">{{ ($mail['mail_date_created']) ?? 'Invalid date' }}</span>
        </p>
      </div>

      <div class="mailbox-controls with-border text-left px-0">
        @include('mail/partials/controls/_read')
      </div>

      <div class="mailbox-read-message" style="border-bottom: 1px solid rgba(0, 0, 0, .125);">{{ $mail['mail_text'] }}</div>
    </div>

    <div class="card-footer px-0">
      @include('mail/partials/controls/_read')
    </div>
  </div>
@endsection

@section('scripts')
  <script>
    $('.mail-print').on({
      click: function () {
        let mail = $('.mailbox-read-info, .mailbox-read-message'), title = mail.find('h5').text();

        mail.printThis({
          header: `<h1>${title}</h1>`
        });
      },
    });
  </script>
@endsection
