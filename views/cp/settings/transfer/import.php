@extends('layout/main')

@section('content')
  <div class="row">
    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
      @if (session('temp') and count(session('temp')) > 0)
        @if (session('temp', 'i') > 0)
          <div class='alert alert-success'>Imported <b>{{ session('temp', 'i') }}</b> rows successfully.</div>
        @elseif (count(session('temp', 'errors')) > 0)
          @foreach (session('temp', 'errors') as $key => $error)
            <div class='alert alert-danger'>{{ $error }}</div>
          @endforeach
        @endif
      @endif

      <form method="post" action="" enctype="multipart/form-data" class='row mb-4'>
        <div class="col-6">
          <select class="select2bs4 form-control" name="table" data-placeholder="Seelct table to import into ..." required>
            <option value="" disabled="disabled" selected="selected"></option>
            @foreach ($tables as $key => $table) {!! option($table, $table) !!} @endforeach
          </select>
        </div>

        <div class="col-6">
          <select class="select2bs4 form-control" name="action" data-placeholder="Select action to do ..." required>
            <option value="" disabled="disabled" selected="selected"></option>
            @foreach (['insert', 'update', 'delete'] as $key => $action) {!! option($action, ucfirst($action)) !!} @endforeach
          </select>
        </div>

        <p class="col-12"></p>

        <div class="col-12">
          <label for='excel_file'>Excel file</label>

          <div class='input-group'>
            <div class='custom-file'>
              <input type='file' name='excel_file' id='excel_file' data-input='excel_file' class='image-input' required
                accept='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' />
              <label class='custom-file-label' data-label='excel_file' for='excel_file'>Choose file</label>
            </div>
            <div class='input-group-append'>
              <button type='button' class='btn btn-muted btn-remove-image-input' data-remove-image-btn='excel_file' disabled>
                <i class='fas fa-times'></i>
              </button>
            </div>
          </div>
        </div>

        <div class="col-12 text-center my-2">
          <button type="submit" class='btn btn-success'><i class="fas fa-file-import mr-1"></i> Import</button>
        </div>
      </form>
    </div>

    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
      <div class="card card-primary card-outline">
        <div class='card-header'>
          <h3 class="card-title">
            <i class="fas fa-info mr-2"></i>
            <span>Importing Hints</span>
          </h3>
        </div>
        <div class='card-body'>
          <ul class='pl-3'>
            <li>Use correct table files to avoid import errors.</li>
            <li>Table files must have primary-key column, even if empty.</li>
            <li>Table file schema must match database table.</li>
            <li>Exercise caution in Delete and Update actions, as file columns will override existing data.</li>
            <li>Lines without primary-key value will be ignored in Delete and Update actions.</li>
            <li>In delete action, only primary-key column with values is needed.</li>
            <li>In Insert action, include primary-key column with empty values and Ornata will generate them.</li>
            <li>To generate an empty template, choose 'Generate blank file' for the desired table on the 'Export' page.</li>
            <li>Before importing a large amount of data, it's a good idea to test the import process with a smaller sample of data to ensure that everything works correctly.</li>
            <li>If you're importing data into a production environment, consider taking a backup of the database beforehand in case something goes wrong during the import process.</li>
            <li>If you're importing data into a table with existing data, make sure to check for any duplicate primary keys before importing to avoid conflicts.</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
@endsection
