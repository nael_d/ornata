@extends('layout/main')

@section('content')
  @php $url = 'settings/apis';  @endphp
  <x-section :items="$items" title="API title" name="belongs_to" column="api" :url="$url" table="apis" limit="10" />
@endsection
