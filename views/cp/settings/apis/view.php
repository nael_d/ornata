@extends('layout/main')

@section('content')
  @php
    $item   ??= [];
    $url    = 'settings/apis';
    $prefix = 'api';
    $href   = count($item) == 0 ? $url : "{$url}/{$item["id_{$prefix}"]}";
    $path   = path("cp/{$href}/store");
    $view   = count($item) == 0 ? 'create' : 'update';
  @endphp

  <section class="content">
    <form enctype="multipart/form-data" method="post" data-view="{{ $view }}" action="{{ $path }}">
      <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8" style="z-index: 2;">
          <x-neutral>
            <x-input title="API Key" :prefix="$prefix" column="key" :item="$item" :is_readonly="true" :value="$api_key" />
            <x-input title="API Identifier" :prefix="$prefix" column="belongs_to" :item="$item" />
            <x-input title="API Password" :prefix="$prefix" column="password" :item="$item" />
            @php $attrs = ['min' => date("Y-m-d", strtotime(date('Y-m-d') . ' +1 days')), 'pattern' => '\d{1,2}/\d{1,2}/\d{4}']; @endphp
            <x-input title="API Expiration" :prefix="$prefix" column="date_expired" :item="$item" :is_required="false" type="date" :attrs="$attrs" />
            <x-select title="Authorized Tables" :prefix="$prefix" column="permissions" :item="$item" :list="$tables" :is_multiple="true" />
            <x-select title="API Activity" :prefix="$prefix" column="is_active" :item="$item" :preset_activity="true" />
          </x-neutral>
        </div>

        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
          <x-hint icon="fas fa-info" title="API Usage">
            <p>To use your API credentials, you'll need to include them in the request headers. To do this, add the following key-value pairs to your headers:</p>
            <h6><kbd>x-{$PREFIX}-api-key:</kbd> API key.</h6>
            <h6><kbd>x-{$PREFIX}-api-password:</kbd> API password.</h6>
            <hr />
            <p class='mb-0'>The variable <kbd>$PREFIX</kbd> is customizable in <kbd>core/Api.php</kbd> file to protect your HTTP requests.</p>
          </x-hint>
        </div>

        <div class="col-12">
          <div class="text-center">
            <button type="submit" class="btn btn-primary btn-lg">
              <i class="fas fa-save mr-1"></i>
              <span>Save</span>
            </button>
          </div>
        </div>
      </div>
    </form>
  </section>
@endsection
