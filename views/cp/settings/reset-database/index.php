@extends('layout/main')

@section('content')
  <section class="content container-narrow text-center mb-3" style="max-width: 768px;">
    <h5 class="h5 text-center mb-4">Caution: Resetting the database will permanently delete all data, including todos, mails, users, and APIs.</h5>
    <h5 class="h5 text-center mb-4">Once reset, you'll be logged out, and the superuser credentials will be <kbd>admin:pass</kbd></h5>
    <h5 class="h5 text-center mb-4">Google Authenticator codes will need to be regenerated. Previous codes for all users will no longer be valid.</h5>
    <h5 class="h5 text-center mb-4">Are you ready to proceed? This action is irreversible, so make it count! 🚀</h5>
    <h5 class="h5 text-center mb-4">Click the action link multiple times to confirm the reset.</h5>
    <h5 class="h5 text-center mb-4">You still have
      <span>
        @if($times > 1)
          {{ $times }}
        @else
          {!! b(u("LAST CHANCE")) !!}
        @endif
        attempt{{ $times > 1 ? "s" : '' }}.
      </span>
    </h5>

    <a href="{{ path('cp/settings/reset-database/hit') }}" class='btn'>
      <i class="fas fa-trash fa-fw"></i>
      <span>Okay, I'm ready to RESET</span>
    </a>
    <a href="{{ path('cp/settings/reset-database/cancel') }}" class='btn btn-success mx-2'>
      <i class="fas fa-life-ring fa-fw"></i>
      <span>Nah, forget that. Get OUT!</span>
    </a>
  </section>
@endsection
