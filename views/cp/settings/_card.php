@php $card = $attributes['card']; @endphp

<a class="card text-center" href="{{ $card['path'] }}">
  <div class="card-body p-0">
    <div class="card-title float-none" style="--card-bg-color: {{ $card['color'] }};">
      <i class="{{ $card['icon'] }}"></i>
    </div>
    <p class="card-text text-xs">{{ $card['text'] }}</p>
  </div>
</a>
