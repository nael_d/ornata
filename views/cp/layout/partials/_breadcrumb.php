<div class="content-header mb-2 container">
  <div class="row">
    <div class="col-12 col-md-7">
      <h1>{{ $title }}</h1>
    </div>
    <div class="col-12 col-md-5">
      <ul class='breadcrumb float-md-right mt-2 mt-md-0'>
        <li class="breadcrumb-item"><a href="{{ path('cp') }}">Dashboard</a></li>
        @php
          $breadcrumb_link = '';
          $breadcrumb_last_item = false;
          $cpa = str_replace('/cp', '', self::$global->current_path);
          $cpa = ltrim($cpa, '/');
          $cpa = $cpa != "" ? explode('/', $cpa) : [];
          // self::$global is in core/CP
        @endphp

        @foreach ($cpa as $key => $link)
          @continue($link == "/")
          @php
            $breadcrumb_last_item = (count($cpa) == ($key + 1)) ? 'active' : '';
            $breadcrumb_link .= "{$link}/";
          @endphp
          <li @class(['breadcrumb-item', $breadcrumb_last_item])>
            @unless($breadcrumb_last_item == 'active')
              {!! a("cp/{$breadcrumb_link}", ucfirst($link), match_url: false, match_url_exact: false) !!}
            @else
              {!! ucfirst($link) !!}
            @endunless
          </li>
        @endforeach
      </ul>
    </div>
  </div>
</div>
