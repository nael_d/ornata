<footer class="main-footer">
  <div class="container">
    <strong>{!! powered_by(env('base_title')) !!}.</strong>
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> {{ file_get_contents("ornata-version") }}
    </div>
  </div>
</footer>
