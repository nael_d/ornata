@extends('layout/partials/form/_formcard')

@section('card-header')
  <i class="fas fa-flag mr-2"></i><span>Neutral</span>
@endsection

@section('card-content')
  {!! $contents !!}
@endsection
