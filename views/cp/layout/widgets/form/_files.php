@extends('layout/partials/form/_formcard')

@section('card-header')
  <i class="fas fas fa-copy mr-2"></i><span>{{ $attributes['title'] ?? 'Files' }}</span>
@endsection

@section('card-content')
  @php
    $item     = is_array($attributes['item']) ? $attributes['item'] : [];
    $prefix   = $attributes['prefix'] ?? '';
    $column   = $attributes['column'] ?? '';
    $storage  = $attributes['storage'] ?? '';
    $table    = $attributes['table'] ?? '';
  @endphp

  @if(count($item) == 0)
    <h6 class="h6 text-center">Files can be uploaded after this item is successfully created.</h6>
  @else
    @php
      if (!function_exists('filedrop_file')) {
        function filedrop_file($file, $folder) {
          $icons = [
            'jpg'   => '<i class="fas  fa-2x fa-fw  fa-image"></i>',
            'jpeg'  => '<i class="fas  fa-2x fa-fw  fa-image"></i>',
            'png'   => '<i class="fas  fa-2x fa-fw  fa-image"></i>',
            'gif'   => '<i class="fas  fa-2x fa-fw  fa-image"></i>',
            'mp3'   => '<i class="fas  fa-2x fa-fw  fa-music"></i>',
            'wav'   => '<i class="fas  fa-2x fa-fw  fa-music"></i>',
            'mp4'   => '<i class="fas  fa-2x fa-fw  fa-film"></i>',
            'avi'   => '<i class="fas  fa-2x fa-fw  fa-film"></i>',
            'pdf'   => '<i class="fas  fa-2x fa-fw  fa-file-pdf"></i>',
            'doc'   => '<i class="fas  fa-2x fa-fw  fa-file-word"></i>',
            'docx'  => '<i class="fas  fa-2x fa-fw  fa-file-word"></i>',
            'rar'   => '<i class="fas  fa-2x fa-fw  fa-file-archive"></i>',
            '*'     => '<i class="fas  fa-2x fa-fw  fa-file"></i>',
          ];

          $icon = $icons[strtolower(pathinfo($file, PATHINFO_EXTENSION))] ?? $icons['*'];
          $download = storage("{$folder}/$file");

          return <<<file
            <div class="filedrop-file" data-id="{$file}">
              <div class="filedrop-file-icon">{$icon}</div>
              <div class="filedrop-file-name">
                <small>{$file}</small>
                <div class="filedrop-tools d-flex w-100 align-items-center" style="gap: 5px">
                  <div class="filedrop-file-tools d-flex" style="gap: 5px">
                    <button type="button" class="filedrop-remove btn btn-danger btn-xs"
                      data-toggle="tippyjs" data-tippy-content="Remove" data-tippy-appendto="parent">
                      <i class="fas fa-trash fa-sm fa-fw"></i>
                    </button>
                    <a href="{$download}" class="filedrop-download btn btn-info btn-xs" target="_blank"
                      data-toggle="tippyjs" data-tippy-content="Download" data-tippy-appendto="parent">
                      <i class="fas fa-download fa-sm fa-fw"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          file;
        }
      }
    @endphp

    <div class="filedrop" data-prefix="{{ $prefix }}" data-column="{{ $column }}" data-table="{{ $table }}" data-id="{{ $item["id_{$prefix}"] }}">
      @foreach($item["{$prefix}_{$column}"] as $key => $file)
        {!! filedrop_file($file, "{$storage}/{$column}") !!}
      @endforeach
    </div>
  @endif
@endsection
