@php
  $css = [
    'cp/AdminLTE-3.2.0/libs/fontawesome-free/css/all.min.css',
    'cp/AdminLTE-3.2.0/libs/bootstrap-v4.6.1/css/bootstrap.css',
    'cp/AdminLTE-3.2.0/libs/summernote-v0.8.20/summernote-bs4.min.css',
    'cp/AdminLTE-3.2.0/libs/overlayScrollbars-v1.13.0/css/OverlayScrollbars.min.css',
    'cp/AdminLTE-3.2.0/libs/icheck-bootstrap/icheck-bootstrap.css',
    'cp/AdminLTE-3.2.0/libs/bootstrap-switch/css/bootstrap3/bootstrap-switch.css',
    'cp/AdminLTE-3.2.0/libs/sweetalert2/sweetalert2.css',
    'cp/AdminLTE-3.2.0/libs/sweetalert2-theme-bootstrap-4/bootstrap-4.css',
    'core/libs/tippyjs/tippy.css',
    'cp/AdminLTE-3.2.0/libs/select2/css/select2.css',
    'cp/AdminLTE-3.2.0/libs/select2-bootstrap4-theme/select2-bootstrap4.css',
    'cp/AdminLTE-3.2.0/libs/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.css',
    'cp/AdminLTE-3.2.0/libs/datatables-2.1.5/datatables.min.css',
    'cp/AdminLTE-3.2.0/css/adminlte.css',
    'cp/AdminLTE-3.2.0/css/adminlte.fixes.css',
    // 'core/css/app.css',
    'cp/css/app.css',
  ];

  $js = [
    'cp/AdminLTE-3.2.0/libs/jquery-v3.6.0/jquery.js',
    'cp/AdminLTE-3.2.0/libs/jquery-v3.6.0/jquery-migrate-3.4.1.min.js',
    // 'cp/AdminLTE-3.2.0/libs/jquery-v3.6.0/jquery-1.12.4.min.js',
    'cp/AdminLTE-3.2.0/libs/bootstrap-v4.6.1/js/bootstrap.bundle.min.js',
    'cp/AdminLTE-3.2.0/libs/jquery-knob/jquery.knob.min.js',
    'cp/AdminLTE-3.2.0/libs/summernote-v0.8.20/summernote-bs4.min.js',
    'cp/AdminLTE-3.2.0/libs/overlayScrollbars-v1.13.0/js/jquery.overlayScrollbars.min.js',
    'core/libs/popperjs@1.16.1/popper.min.js',
    'cp/AdminLTE-3.2.0/libs/jasonday-printThis/printThis.js',
    'cp/AdminLTE-3.2.0/libs/bootstrap-switch/js/bootstrap-switch.js',
    'cp/AdminLTE-3.2.0/libs/sweetalert2/sweetalert2.all.min.js',
    'core/libs/tippyjs/tippy.js',
    'cp/AdminLTE-3.2.0/libs/select2/js/select2.full.min.js',
    'cp/AdminLTE-3.2.0/libs/moment/moment.min.js',
    'cp/AdminLTE-3.2.0/libs/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js',
    'cp/AdminLTE-3.2.0/libs/datatables-2.1.5/datatables.min.js',
    'cp/js/jquery-filedrop-master/jquery.filedrop.js',
    'cp/AdminLTE-3.2.0/js/adminlte.js',
    'cp/js/app.js',
  ];
@endphp

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="icon" type="image/jpg" href="{{ asset('cp/pics/logo.png') }}" />
  @foreach ($css as $key => $value){!! css($value) !!}  @endforeach
  <title>{{ $title ?? "CP" }} :: {{ env('base_title') }} CP</title>
</head>
<body @class(['sidebar-mini', 'control-sidebar-slide-open', 'layout-navbar-fixed', 'layout-fixed', 'sidebar-collapse', 'layout-footer-fixed', 'text-sm', 'dark-mode' => self::$global->darkmode == 1])>
  <div class="wrapper">
    @include('layout/partials/_header')

    <div class="content-wrapper">
      @include('layout/partials/_breadcrumb')

      <section class='content container mb-3-'>
        @yield('content')
      </section>
    </div>

    @include('layout/partials/_footer')
  </div>

  @foreach ($js as $key => $value){!! js($value) !!}  @endforeach
  @yield('private-scripts') <?php // this is another scripts yield because the first scripts yield might be used. ?>
  @yield('scripts')
</body>
</html>