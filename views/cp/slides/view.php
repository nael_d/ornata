@extends('layout/main')

@section('content')
  @php
    $item   ??= [];
    $url    = 'slides';
    $prefix = 'slide';
    $href   = count($item) == 0 ? $url : "{$url}/{$item["id_{$prefix}"]}";
    $path   = path("cp/{$href}/store");
    $view   = count($item) == 0 ? 'create' : 'update';
  @endphp

  <section class="content">
    <form enctype="multipart/form-data" method="post" data-view="{{ $view }}" action="{{ $path }}">
      <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8">
          <x-polyglot>
            @php $i = 0; @endphp
            @foreach (self::$global->langs as $language => $lang)
              @php $i++; @endphp
              <div role="tabpanel" id="vert-tabs-{{ $lang }}" aria-labelledby="vert-tabs-{{ $lang }}-tab" @class(['tab-pane', 'text-left', 'fade', 'active show' => $i == 1])>
                <x-input title="Title" :prefix="$prefix" column="title" :lang="$lang" :item="$item" />
              </div>
            @endforeach
          </x-polyglot>

          <x-neutral>
            <x-select title="Activity" :prefix="$prefix" column="is_active" :item="$item" :preset_activity="true" />
          </x-neutral>
        </div>

        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
          <div class="row">
            <div class="col-12 col-md-6 col-lg-12">
              <x-image title="Main Image" :prefix="$prefix" column="image" storage="slides" :item="$item" :is_required="true" />
            </div>
          </div>
        </div>

        <div class="col-12">
          <div class="text-center">
            <button type="submit" class="btn btn-primary btn-lg">
              <i class="fas fa-save mr-1"></i>
              <span>Save</span>
            </button>
          </div>
        </div>
      </div>
    </form>
  </section>
@endsection
