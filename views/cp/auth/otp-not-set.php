@extends('layout/auth')

@section('content')
  <div class="login-box-msg text-center">
    <small>Use Google Authenticator to register your account and gain your own OTP code.</small>
    <div class="row">
      <div class="col-6">
        {!! a('https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2', img(asset('cp/pics/auth/google-play.png'), 'd-block w-100 my-2'), 'd-block w-100', true) !!}
      </div>
      <div class="col-6">
        {!! a('https://apps.apple.com/us/app/google-authenticator/id388497605?platform=iphone', img(asset('cp/pics/auth/app-store.png'), 'd-block w-100 my-2'), 'd-block w-100', true) !!}
      </div>
    </div>
    <hr class="my-2" />
    <small>Once you've done, scan this QR code and enter the OTP code to verify and login.</small>
    <div><img src="{{ $img }}" class="mt-3" /></div>
    {{-- <h2>{{ $secret }}</h2> --}}
  </div>
  <a href="{{ self::$global->current_path_full }}" class="btn btn-primary btn-block">Verify OTP</a>
@endsection
