<?php

namespace CpComponent;

class Backuprow extends \Corviz\Crow\Component {
  public function __construct() {
    parent::__construct();
    // $this->extension = '';
  }

  /**
   * @inheritDoc
   */
  public function render(): void {
    $this->view('settings/backups/_row');
  }
}
