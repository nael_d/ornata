<?php

namespace CpComponent;

class Settingcard extends \Corviz\Crow\Component {

  public function __construct() {
    parent::__construct();
    // $this->extension = '';
  }

  /**
   * @inheritDoc
   */
  public function render(): void {
    $this->view('settings/_card');
  }
}
