<?php

namespace CpComponent;

class Cronjobrow extends \Corviz\Crow\Component {
  public function __construct() {
    parent::__construct();
    // $this->extension = '';
  }

  /**
   * @inheritDoc
   */
  public function render(): void {
    $this->view('settings/cronjobs/widgets/_row');
  }
}
