<?php

namespace CpComponent;

class Weather extends \Corviz\Crow\Component {

  public function __construct() {
    parent::__construct();
    // $this->extension = '';
  }
  
  /**
   * @inheritDoc
   */
  public function render(): void {
    $this->view('home/widgets/_weather');
  }
}