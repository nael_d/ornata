<?php

namespace CpComponent;

class Infobox extends \Corviz\Crow\Component {

  public function __construct() {
    parent::__construct();
    // $this->extension = '';
  }

  /**
   * @inheritDoc
   */
  public function render(): void {
    $this->view('maillist/widgets/_infobox');
  }
}
