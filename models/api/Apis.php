<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

// namespace WebModel|CpModel|ApiModel
namespace ApiModel;

class Apis Extends \Model {

  public function __construct() {
    parent::__construct('apis', 'api', '');
    // $this->activeOnly = false;
  }

}