<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

namespace CpModel;

class Maillist Extends \Model {

  public function __construct() {
    parent::__construct('maillist_subs', 'maillist_sub', '');
    $this->activeOnly = false;
    $this->i18n       = [];
    $this->neutral    = ['email', 'ip', 'useragent'];
  }

  public function subscribe($email) {
    // check that if $email is exist or no
    $subscription = $this->get(where: ['maillist_sub_email' => $email]);

    if (count($subscription) == 0) {
      // not subscribed before.
      $this->store([
        'maillist_sub_email'      => $email,
        'maillist_sub_ip'         => ip(),
        'maillist_sub_useragent'  => $_SERVER["HTTP_USER_AGENT"],
      ]);
    }
    else {
      $subscription = current($subscription);
      if ($subscription['maillist_sub_is_active'] == 0) {
        // unsubscribed by user. so resubscribe.
        $this->neutral = ['is_active'];
        $this->store(['maillist_sub_is_active' => 1], id: $subscription['id_maillist_sub']);
      }
      else {
        // already subscribed. do nothing.
      }
    }

    return true;
  }

  public function unsubscribe($email) {
    // check that if $email is exist or no
    $subscription = $this->get(where: ['maillist_sub_email' => $email]);

    if (count($subscription) == 0) {
      // not subscribed before. do nothing.
    }
    else {
      $subscription = current($subscription);
      $this->neutral = ['is_active'];
      $this->store(['maillist_sub_is_active' => 0], id: $subscription['id_maillist_sub']);
    }

    return true;
  }

  public function get_subs() {
    $this->activeOnly = true;
    return $this->get();
  }

  public function get_subs_count() {
    $this->activeOnly = true;
    return count($this->get());
  }

  public function get_mails(int $type = 0, int|null $id = null) {
    /**
     * $type = [
     *  0 => is_sent = 0    => not sent yet (scheduled)
     *  1 => is_sent = 1    => sent
     *  2 => is_deleted = 1 => deleted by user before sending
     * ]
     */

    $this->set('maillist_mails', 'maillist_mail');
    $where = [];

    if ($id > 0) { return parent::get(id: $id); }
    else if (in_array($type, [0, 1])) { $where['maillist_mail_is_sent'] = $type; }
    else if ($type == 2) { $where['maillist_mail_is_deleted'] = 1; }

    return parent::get(where: $where);
  }

  public function save_mail($mail, $id = null) {
    $this->set('maillist_mails', 'maillist_mail');
    $this->neutral = ['title', 'message', 'date_scheduled'];
    $now = strtotime("now");
    $sch = strtotime(str_replace('/', '-', $mail['maillist_mail_date_scheduled']));

    if ($sch <= $now) {
      // the user didn't save the mail instantly and the scheduled time has become more than the $now value.
      // so, let's correct the $sch value
      $sch = $now + 600; // 600 => 10 minutes
    }

    $inputs = [...$mail, 'maillist_mail_date_scheduled'  => $sch];

    if ($id == null or ($id > 0 and $inputs['maillist_mail_is_sent'] == 0)) {
      // save 
      parent::store(id: $id, inputs: $inputs);
      return true;
    }
  }

  public function mark_mail_as_sent($id) {
    $this->set('maillist_mails', 'maillist_mail');
    $this->neutral = ['is_sent'];

    return parent::store(inputs: ['maillist_mail_is_sent' => 1], id: $id);
  }

}