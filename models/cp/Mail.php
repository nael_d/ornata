<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

// namespace WebModel|CpModel|ApiModel
namespace CpModel;

class Mail Extends \Model {

  public function __construct() {
    parent::__construct('mail', 'mail');
    $this->neutral  = ['is_read'];
    $this->i18n     = [];
  }

  public function get_mails(string $status = 'inbox') {
    $w = match ($status) {
      'unread'  => ['mail_is_read' => 0],
      'trash'   => ['mail_is_active' => 0],
      default   => [],
    };

    return $this->get(where: $w);
  }

  public function set_mail_read(int $id_mail) {
    $this->store(inputs: ['mail_is_read' => 1], id: $id_mail);
  }

  public function toggle_mail(int $id_mail, string $status) {
    $this->neutral = ['is_active']; // force set the only-one needed key to update.
    $this->store(inputs: ['mail_is_active' => $status == 'delete' ? 0 : 1], id: $id_mail);
  }

  public function send(array $mail) {
    $this->neutral = ['title', 'sender', 'sender_email', 'sender_phone', 'text', 'is_read', 'is_active'];
    return $this->store([
      'mail_title'        => $mail['title'],
      'mail_sender'       => $mail['sender'],
      'mail_sender_email' => $mail['sender_email'],
      'mail_sender_phone' => $mail['sender_phone'],
      'mail_text'         => $mail['text'],
      'mail_is_read'      => 0,
      'mail_is_active'    => 1,
    ]);
  }

}
