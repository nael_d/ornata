<?php

/**
 * Create a copy from this basic plain controller and use it
 * wherever you need in the specific folder.
 * 
 * You have to set the namespace depending on its usage and
 * folder location below.
 */

// namespace WebModel|CpModel|ApiModel
namespace CpModel;

class Settings Extends \Model {

  public function __construct() {
    parent::__construct('settings', 'setting', 'settings');
    $this->activeOnly = false;
    $this->i18n       = [];
    $this->neutral    = ['value'];
  }

  public function preference_get(string $key) {
    $pref = $this->get(where: ['setting_key' => $key]);

    if (count($pref) == 0) return [];
    $pref = current($this->get(where: ['setting_key' => $key]));
    return $pref;
  }

  public function preference_get_value(string $key) {
    $pref = $this->preference_get($key);
    return $pref['setting_value'];
  }

  public function preference_set(string $key, string|int|array|object $value) {
    $pref = $this->preference_get($key);

    if (count($pref) == 0) return;
    return parent::store(['setting_value' => $value], [], $pref['id_setting']);
  }

  public function preference_activate(string $key) {
    if (count($this->preference_get($key)) == 0) return null;
    return $this->store(['setting_is_active' => 1], id: $key) ? true : false;
  }

  public function preference_deactivate(string $key) {
    if (count($this->preference_get($key)) == 0) return null;
    return $this->store(['setting_is_active' => 0], id: $key) ? true : false;
  }

  public function preference_toggle(string $key) {
    $pref = $this->preference_get($key);
    if (count($pref) == 0) return null;
    return $this->store(['setting_is_active' => ! (int) $pref['setting_is_active']], id: $key) ? true : false;
  }

  public function store(array $inputs = [], array $files = [], string|int|null $id = null) {
    $setting = $this->preference_get($id);
    $value = [];

    foreach ($inputs as $k => $input) {
      $input = sanitizer($input);

      if ($this->use_encrypia == 1) {
        $input = \Encrypia::blind($input);
      }

      if ($k == 'setting_is_active') { continue; } // to skip setting_is_active key if passed
      $k_trimmed = str_replace($this->col . '_value-', '', $k);

      if (strpos($k_trimmed, '_')) {
        // setting_value_[key_multiple_words?][languages?]
        $k_exploded = explode('_', $k_trimmed);
        $matched = array_intersect($k_exploded, (array)$this->langs);

        if (count($matched) > 0) {
          // setting_value_[key_multiple_words][languages]
          // it means that this field have to be treated as multi languages
          $value[implode('_', array_diff($k_exploded, $matched))][current($matched)] = $input;
        }
        else {
          // setting_value_[key_multiple_words]
          $value[implode('_', array_diff($k_exploded, $matched))] = $input;
        }
      }
      else {
        // setting_value_[key]
        $value[$k_trimmed] = $input;
      }
    }

    if (array_key_exists('setting_is_active', $inputs)) {
      $this->neutral[] = 'is_active';
    }

    $inputs[$this->col . '_value'] = json_encode($value, JSON_UNESCAPED_UNICODE);
    return parent::store($inputs, $files, is_int($id) ? $id : $setting['id_setting']);
  }

  public function activate_website() {
    $setting          = $this->preference_get_value('website-status');
    $activation_date  = value_json($setting, 'activation_date');
    $status           = value_json($setting, 'status');
    $now              = strtotime("now");
    $sch              = strtotime(str_replace('/', '-', $activation_date));

    if ($status == 0) {
      if ($sch <= $now) {
        /**
         * scheduled date is invalid: set into the past from now.
         * so, we'll activate the website and ignore it.
         */
        $status = 1;
      }
    }

    if ($status == 1) {
      $setting->status = 1;
      return $this->preference_set('website-status', json_encode($setting)) ? true : false;
    }

    return false; // not activated. this is useful in the cronjob logging.
  }

}