<!DOCTYPE html>
<html lang="en" dir="ltr">

  <head>
    <meta charset="UTF-8" />
    <base href="http://<?php echo $_SERVER['HTTP_HOST'] . "/"; ?>" />
    <script>
      if (`${location.origin}${location.pathname}` !== document.querySelector('base').getAttribute('href')) {
        location.href = document.querySelector('base').getAttribute('href');
      }
    </script>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="pics/ornata-ico.png" />
    <link rel='stylesheet' href='css/layout.css' />
    <link rel='stylesheet' href='css/app.css' />
    <title>Welcome :: Ornata Documentation</title>
  </head>

  <body class='welcome-page'>
    <header style='opacity: 0;'>
      <div class="container">
        <div class="row justify-content-between">
          <div class="col-auto d-flex align-items-center">
            <button class='aside-trigger'><i class="fas fa-bars d-block"></i></button>
            <a href='' class='header-logo' data-faster-link='/'><img alt="" src="pics/ornata-text-1.png" /></a>
          </div>
          <div class="col-auto">
            <a href='https://gitlab.com/nael_d/ornata' class='color-1-text gitlab-link' target='_blank'
              data-toggle='tippyjs' data-tippy-content='Ornata on Gitlab'><i class="fab fa-gitlab"></i></a>
            <a href='https://discord.gg/U7qv7SMJ8H' class='color-8-text gitlab-link' target='_blank'
              data-toggle='tippyjs' data-tippy-content='Ornata on Discord'><i class="fab fa-discord"></i></a>
            <a href='' class='cta-main' style='position: relative; top: 50%; translate: 0 -50%;'
              data-faster-link='/get-started'>Get started</a>
          </div>
        </div>
      </div>
    </header>

    <div class="aside-container">
      <aside>
        <a href='' class='header-logo' data-faster-link='/'>
          <img alt="" src="pics/ornata-text-1.png" />
        </a>
        <ul></ul>
      </aside>
    </div>

    <main data-faster-app style='opacity: 0;'></main>

    <div
      data-faster-loading
      style="display: flex; flex-direction: column; justify-content: center; align-items: center; gap: 20px;
        z-index: 3; position: fixed; top: 0; left: 0; width: 100%; height: 100%; background-color: white;">
      <div class="logos-row d-flex" style="gap: 16px;">
        <img src="pics/ornata-logo-2.png" alt="Ornata logo" style="max-height: 85px;" />
        <img src="pics/ornata-text-1.png" alt="Ornata word" style="max-height: 85px;" />
      </div>
      <div class="ornata-loader"></div>
    </div>

    <footer class="pb-3 pt-2" style='opacity: 0;'>
      <div class="container">
        <div class='text-muted text-center'>
          <a href='https://www.ornata.dev' target='_blank'>Ornata</a> website is powered by OrnataJS, a beta sub-project of Ornata.
        </div>
        <div class='text-muted text-center mt-1'>
          <small>Developed with <i class="fas fa-heart"></i> by <a href='http://naeldahman.me' class='color-5-text'
              target='_blank'>Nael Dahman</a> for a better web.</small><br />&copy; 2022-<?=date('Y')?>
        </div>
      </div>
    </footer>

    <script>let ornataVersion = "<?php echo file_get_contents('./ornata-version'); ?>";</script>
    <script src='js/layout.js'></script>
    <script src='js/documentation.js'></script>
  </body>

</html>
