@echo off
setlocal enabledelayedexpansion

@REM Get the directory path containing Cli.php
set CLI_DIRECTORY=%~dp0

@REM Set the current directory to that project's path (in some cases, it is C:\WINDOWS\system32 which leads to fail)
cd /d !CLI_DIRECTORY!

@REM User-defined PHP folder path
set PHP_PATH=

@REM Check if .env file exists
if exist .env (
  REM Load PHP_PATH from .env file
  for /f "tokens=1,* delims== " %%A in ('findstr /r "^php_path=" .env') do (
    set PHP_PATH=%%B
  )
) else (
  echo Ornata CLI Setup Incomplete
  echo -------------------------------------------------------------------------
  echo Ornata CLI needs some basic values that must be set in the `.env` file.
  echo Please complete the Ornata Setup process to have `ornata` commad ready
  echo for you in your current project.
  echo -------------------------------------------------------------------------
  echo.
  pause
  exit /b
)

@REM Convert backslashes to forward slashes for compatibility
@REM set "CLI_DIRECTORY=!CLI_DIRECTORY:\=//!"

REM Check if running as administrator
NET SESSION >nul 2>&1
if %errorLevel% == 0 (
  set "ADMIN=1"
) else (
  set "ADMIN=0"
)

@REM If running as administrator, proceed with path addition
if "%ADMIN%"=="1" (
  REM Check if the directory is already in the user or system PATH
  set PATH_EXISTS=
  for %%I in ("%PATH:;=" "%") do (
    set "path_part=%%~I"
    set "path_part=!path_part:"=!"
    @REM set "path_part=!path_part:\=//!"

    if /I "!path_part!"=="!CLI_DIRECTORY!" set "PATH_EXISTS=true"
  )

  if not defined PATH_EXISTS (
    @REM Decided not to add the current project's path into PATH,
    @REM because `ornata` command is available at the root of the project.
    @REM But decided to keep the code for any later necessary. Who knows? :-D

    @REM Add the directory to the system PATH
    set "NEW_PATH=!PATH!;%CLI_DIRECTORY%\"
    @REM setx PATH "!NEW_PATH!" /M

    @REM Add the directory to the user PATH
    setx PATH "!NEW_PATH!"

    @REM set PATH=!NEW_PATH!
    echo Ornata CLI added to your user PATH
    echo.
    pause
  ) else (
    @REM echo "Ornata CLI already exists in PATH"
  )
)

@REM Run the ornata script with any command-line arguments
%PHP_PATH%\php.exe %CLI_DIRECTORY%\core\Cli.php %*
